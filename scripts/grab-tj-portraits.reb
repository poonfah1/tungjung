Rebol [
	title: "Grab portrait files from TJ Website"
	date: 9-Jan-2020
	author: "Graham Chiu"
	name: %grab-tj-pdfs.reb
	notes: {
		use: r3 grab-tj-portraits.reb

		and this downloads all the portraits from https://www.tungjung.org.nz/portraits?start=0
		to the local directory
		sample data to parse:

		<div class="col-sm-2 mt-sm-1">
            <div class="card portrait-card">
                
                <img src="/assets/portrait/F174184__FillMaxWzEwMCwxMjVd.jpg" class="card-img-top" alt="YIU Che Lam">
                
                <div class="card-body">
                    <h6 class="card-title">YIU Che Lam</h6>
                    <h6 class="card-title"></h6>
                    <p class="card-text">174184</p>
                    <a href="/portraits/show/1708" class="btn btn-primary">Read More</a>
                </div>
            </div>
        </div>
	}
]

src: https://www.tungjung.org.nz/portraits?start=
base: https://www.tungjung.org.nz

lc: 0

for counter 0 168 24 [ ; go through each page
	data: to-text read join src counter
	parse data [some [
		to {<img src="/assets/portrait/} thru {<img src="} copy link to {"} thru ">" (
			if %.jpg = suffix? link [
				attempt [
					name: to-file last split-path link
					dump name
					if #"/" = first link [
						download: join base link
					] else [
						download: join src link
					]
					print spaced ["Reading" download]
					write name read to-url download
				]
			]
		) | 
		( lc: me + 1 print spaced ["not a portrait link" lc] )
		to {<a href="} thru ">"
	]]
]

print "Finished"