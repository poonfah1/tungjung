Rebol [
	title: "Grab files from TJ Website"
	date: 9-Jan-2020
	author: "Graham Chiu"
	name: %grab-tj-pdfs.reb
	notes: {
		use: r3 grab-tj-pdfs.reb

		and this downloads all the PDFs from https://www.tungjung.org.nz/newsletters
		to the local directory
	}
]

src: https://www.tungjung.org.nz/newsletters
base: https://www.tungjung.org.nz

data: to-text read src
lc: 0

parse data [some [
	thru {<a href="} copy link to {"} (dump link) thru ">" (
		if %.pdf = suffix? link [
			attempt [
				name: to-file last split-path link
				dump name
				if #"/" = first link [
					download: join base link
				] else [
					download: join src link
				]
				print spaced ["Reading" download]
				write name read to-url download
			]
		]
	) | 
	( lc: me + 1 print spaced ["not a link" lc] )
	to {<a href="} thru ">"
]]
