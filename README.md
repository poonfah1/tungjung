![Build Status](https://gitlab.com/pages/zhaoshirong/badges/master/build.svg)

---

This is a VuePress website using GitLab Pages.

Learn more about GitLab Pages at https://about.gitlab.com/product/pages and the official
documentation https://docs.gitlab.com/ee/user/project/pages/.

---

If you want to make changes, then create a pull request.

Contact a Committee member for getting access.

Mark up is done using [Markdown](https://www.markdownguide.org/cheat-sheet/) 
