# New Year of the Rat (2020)

We had a great time at the [Grand Century Restaurant](https://www.facebook.com/pages/Grand-Century/191882514169818) in Te Aro 

## Videos


<iframe class="embed-responsive-item" src="https://youtube.com/embed/7ZAAFk4e0dI" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Grace Yuan Yuan Chen sang two songs for us.  She previously had sung for the President of The People's Republic of China.  _My Heart Will Go On_ by Celine Dion at 00:40, and _Tian Mi Mi_ by Teresa Teng (鄧麗君) at 6:07


<iframe class="embed-responsive-item" src="https://youtube.com/embed/V2BbeJIyYrw" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
