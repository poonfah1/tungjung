# Association Newsletters

Please note that you will need a PDF reader, such as [Adobe Acrobat Reader](https://get.adobe.com/reader/), or [WPS Office](https://www.wps.com/), to read these newsletters.  WPS Office Software is a subsidiary of Kingsoft Corporation, a leading Chinese Software company.


## 2024

| Season			| Download Link |
|:------------------|:--------------|
| Autumn			| [5.1mb](assets/Autumn_2024_issue.pdf)  |

## 2023

| Season			| Download Link |
|:------------------|:--------------|
| Summer			| [5.63mb](assets/Summer_2023_issue.pdf)  |
| Spring			| [4.89mb](assets/Spring_2023_issue.pdf)  |
| Autumn			| [7.84mb](assets/Autumn_2023_issue.pdf)  |
| Winter			| [5.39mb](assets/Winter_2023_issue.pdf)  |


## 2022

| Season			| Download Link |
|:------------------|:--------------|
| Summer			| [7.1mb](assets/Summer_2022_issue.pdf)  |
| Spring			| [6.41mb](assets/Spring_2022_issue.pdf)  |
| Winter			| [5.57mb](assets/Winter_2022_issue.pdf)  |
| Autumn			| [7.36mb](assets/Autumn_2022_issue.pdf)  |

## 2021

| Season			| Download Link |
|:------------------|:--------------|
| Summer			| [5.67mb](assets/Summer_2021_issue.pdf)  |
| Spring			| [4.16mb](assets/Spring_2021_issue.pdf)  |
| Winter			| [5.36mb](assets/Winter_2021_issue.pdf)  |
| Autumn			| [5.78mb](assets/Autumn_2021_issue.pdf)  |


## 2020

| Season			| Download Link |
|:------------------|:--------------|
| Summer			| [5.31mb](assets/Summer_2020_issue.pdf)  |
| Spring			| [5.85mb](assets/Spring_2020_issue.pdf)  |
| Winter            | [3.46mb](assets/Winter_2020_issue.pdf)  |
| Autumn			| [7.70mb](assets/Autumn_2020_issue.pdf)  |


## 2019

| Season			| Download Link |
|:------------------|:--------------|
| Summer			| [7.65mb](assets/Summer-issue-2019-v2.pdf) 	|
| Spring			| [4.13mb](assets/Spring-2019-issue.pdf) 	|
| Winter			| [5.30mb](assets/Winter_2019_issue.pdf) 	|
| Autumn			| [5.20mb](assets/Autumn_2019_issue.pdf)  |

## 2018

| Season			| Download Link |
|:------------------|:--------------|
| Summer			| [4.93mb](assets/Summer_2018_issue.pdf) 	|
| Spring			| [6.25mb](assets/Spring_2018_issue.pdf) 	|
| Winter			| [4.84mb](assets/Winter_2018_issue_pub.pdf) 	|
| Autumn			| [5.20mb](assets/Autumn_2018_issue.pdf)  |


## 2017

| Season			| Download Link |
|:------------------|:--------------|
| Summer			| [4.74mb](assets/Summer_2017_issue_pub.pdf) 	|
| Spring			| [5.16mb](assets/Spring_2017.pdf) 	|
| Winter			| [5.93mb](assets/Winter_2017_issue_.1_pub.pdf) 	|
| Autumn			| [5.69mb](assets/Autumn_2017.pdf)  |


## 2016

| Season			| Download Link |
|:------------------|:--------------|
|Autumn| [8.18Mb](assets/Autumn_issue_2016.pdf) |
|Spring| [6.07Mb](assets/Spring2016_issue.pdf) |
|Summer| [5.55Mb](assets/Summer_2016.pdf) |
|Winter| [6.05Mb](assets/Winter_2016_issue.pdf) |

## 2015

| Season			| Download Link |
|:------------------|:--------------|
|Autumn| [4.28Mb](assets/Autumn_2015_issue.pdf) |
|Spring| [4.69Mb](assets/Spring_2015_issue.pdf) |
|Summer| [2.68Mb](assets/Summer_issue_2015.pdf) |
|Winter| [3.97Mb](assets/Winter_2015_issue.pdf) |

## 2014

| Season			| Download Link |
|:------------------|:--------------|
|Spring| [4.77Mb](assets/Spring_2014_issue.pdf) |
|Summer| [5.29Mb](assets/Summer_2014_issue.pdf) |
|Winter| [3.27Mb](assets/Winter_2014_issue.pdf) |

## 2013

| Season			| Download Link |
|:------------------|:--------------|
|Autumn| [4.16Mb](assets/Autumn_issue_2013.pdf) |
|Spring| [5.21Mb](assets/Spring_2013_issue.pdf) |
|Summer| [2.97Mb](assets/Summer_issue_2013_.pdf) |
|Winter| [6.77Mb](assets/Winter_issue_2013.pdf) |

## 2012

| Season			| Download Link |
|:------------------|:--------------|
|Autumn| [5.18Mb](assets/Autumn_issue_2012.pdf) |
|Spring| [3.69Mb](assets/Spring_2012_issue.pdf) |
|Summer| [5.18Mb](assets/Summer_issue_2012_amended.pdf) |
|Winter| [2.18Mb](assets/Winter_issue_2012.pdf) |

## 2011

| Season			| Download Link |
|:------------------|:--------------|
|Autumn| [4.55Mb](assets/Autumn_issue_2011.pdf) |
|Spring| [2.18Mb](assets/Spring_issue_2011_pdf.pdf) |
|Summer| [2.36Mb](assets/Summer_Issue_2011.pdf) |
|Winter| [4.08Mb](assets/Winter_issue_2011_pdf.pdf) |

## 2010

| Season			| Download Link |
|:------------------|:--------------|
|Autumn| [5.11Mb](assets/Autumn_Issue_2010_amended.pdf) |
|Spring| [4.24Mb](assets/Spring_issue_2010.pdf) |
|Summer| [4.85Mb](assets/Summer_issue_2010.pdf) |
|Winter| [3.55Mb](assets/winter_2010.pdf) |

## 2009

| Season			| Download Link |
|:------------------|:--------------|
|Winter| [3.49Mb](assets/Complete_winter_2009_edition.pdf) |
|Summer| [9.49Mb](assets/Summer_2009.pdf) |
|Autumn| [3.49Mb](assets/Tung_Jung_Autumn_Newsletter2009_print.pdf) |
|Autumn| [0.43Mb](assets/Tung_Jung_Autumn_Newsletter2009_screen.pdf) |

## 2003 - 2008

|Year | Month			| Download Link |
|-----|:------------------|:--------------|
|2008 | July  | [1.17Mb](assets/TJA_Newsletter_July_2008.pdf) |
|2006 | March | [1.91Mb](assets/TungJung_Newsletter_Mar2006.pdf) |
|2005 | December | [47kb](assets/TungJung_Newsletter_Dec2005.pdf) |
|2005 | September | [53Kb](assets/TungJung_Newsletter_Sep2005.pdf.pdf) |
|2005 | July | [1.84Mb](assets/TungJung_Newsletter_Jul2005.pdf) |
|2005 | March | [107Kb](assetsTungJung_Newsletter_Mar2005.pdf) |
|2004 | September | [48Kb](assets/TungJung_Newsletter_Sep2004.pdf) |
|2003 | March | [0.49Mb](assets/TungJung_Newsletter_Mar2003.pdf) |

You can save these newsletters and downloads by right-clicking on the links below and selecting "Save link as...", and then opening them once they have downloaded.  Many browsers will open them once they are downloaded.
