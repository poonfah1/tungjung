# 2021 MEETINGS AND EVENTS

- New Year's Dinner Aries Restaurant Feb 21, 2021

- AGM Sunday 15th August 2021 at 2pm Tung Jung Rooms at 33 Torrens Tce 

## Executive Committee Meetings

Executive Committee meetings usually start at 7:30 pm on the first Monday of each month, commencing 1st Feb 2021

# Social

- Hokianga Ventnor Memorial Unveiling ( April 9-11, 2021)  
   Need to book accommodation now.  Contact [Gordon Wu](/contact/#gordon-wu).

- 95th Celebration Meeting 6th June 2021 (Queen's Birthday Weekend)

- Mid-Winter Yum Chat @ Dragon's Restaurant 21st July 2021 12PM
  Contact [Social Committee](/contacts/#social-committee) for tickets @ $20pp
