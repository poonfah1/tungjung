# Chinese Language Classes (online/physical)

Cantonese Class -no teacher appointed  
Mandarin Class restarts 12th January, 2024 

## Cantonese Classes

### Virtual

No classes until a replacement teacher is found for the late Gordon Wu.

### Physical

No classes until a replacement teacher is found for the late Gordon Wu.

## Mandarin Classes

Beginner's class is online on Fridays at 7:30 pm.  Contact [Graham Chiu](/contact/#vice-president) for the details.  Classes are limited to 3 students.

