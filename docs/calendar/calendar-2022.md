# 2022 MEETINGS AND EVENTS

- New Year's Dinner Dragon's Restaurant Sunday Feb 6, 2022

- AGM (TBA) 2022 at 2pm Virtually by Zoom/Meet 

## Executive Committee Meetings

Executive Committee meetings usually start at 7:30 pm on the first Monday of each month, commencing 7th Feb 2022

# Social

- Ching Ming Sunday 10th April 2022

- Mid-Winter Yum Cha @ Dragon's Restaurant 15th June 2022 12PM
  Contact [Social Committee](/contacts/#social-committee) for tickets @ $25pp (no cheques accepted)

- Moon Festival Sunday 11th Septmeber Dragon's Restaurant

- Chung Yeung Sunday 9th October 2022 (Actual calendar date is 4th October Gregorian Calendar for the Double 9 festival, or 9th day of the 9th month of the Lunar Calendar)

- Christmas Seniors' Yum Cha Wednesday 7 December 2022 12:00 pm Dragon's Restaurant
