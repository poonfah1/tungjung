# The Executive Committee for 2024 - 2025
 

| Office         | Name                         |
|-------------------|--------------------------------------|
| President  主席 | Robert Ngan  颜繼堂   |
| Vice President 副主席 | Robin Young  楊錫彬   |
| Treasurer  財務主管 | Yat Ching Zhou 周日晴 |
| Asst Treasurer 財務助理  | Allen Tso  曹汝安|
| English Secretary  英文秘書 | Murphy Wong 王志强       
| Asst Secretary  助理秘書  | Charlie Ding 鄧家業  |
| Chinese Secretary  中文秘書| Bill Wong  王瑞威  

## Subcommittees:

| Committee         | Name                         |
|-------------------|--------------------------------------|
| Member | Hao You Zhou  周浩攸|
| Member | Zian Liang Zhou  周展良
| Member | Sik Tim Young  楊錫添|
| Member | Ho Ping Joe  周浩平|
| Website| Graham Chiu 趙世榮 (co-opted) |
