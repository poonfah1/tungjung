# Current Events

## NZCA Academic Awards application forms now available, Easter Tournament 2020 cancelled

[Download 2020 Application Form](assets/WCA_March_2020_E-Letter.pdf) 


## Wellington City Council Fireworks Display

~~Sat Feb 01 2020 at 09:30 pm to 10:00 pm~~  postponed due to wind

~~New time of Sun Feb 02 2020 at 09:00 pm to 9:30 pm~~

New New time of Friday Feb 07 2020 at 9:30 pm to 10:00 pm

Check the WCC [twitter feed](https://twitter.com/WgtnCC?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor) to confirm

Frank Kitts Park, Wellington, New Zealand, Wellington

[more...](https://allevents.in/wellington/cny-festival-fireworks/200018442047344)

## Waitangi Day Celebrations 5th, 6th Feb 2020

For [Wellington](https://wellington.govt.nz/events/annual-events/summer-city/waitangi-day)

## ~~Seyip New Year Dinner~~


Feb 6th at Grand Century.  Contact by [email](seyipnz@gmail.com) to get a ticket

## ~~Festivals of Chinese New Year~~

Now **CANCELLED** presumably due to the [Covid-19](https://www.who.int/emergencies/diseases/novel-coronavirus-2019/travel-advice) virus outbreak, and travel restrictions

### Location:

Thu 6 Feb 2020 @ 7:00 pm | The Opera House Wellington, Wellington, NZ

![](./assets/ChineseNewYearShow1.jpg)

![](./assets/ChineseNewYearShow2.jpg)

A Chinese troupe is performing in Wellington at the Opera House on the 6 February beginning at 7 pm.

Admission is by tickets only and are obtainable from [Gordon Wu](/contact/#gordon-wu) at $30 per ticket (a discount is available on group bookings of 12 tickets) in the stalls and $20 per ticket in the circle.

Email or call [Gordon Wu](/contact/#gordon-wu) as these tickets are limited.

Tickets also at [Ticketmaster](https://www.ticketmaster.co.nz/event/2400578A80A8162E) but a $5 surcharge applies for purchase over their website

## Being Chinese in Aotearoa: A Photographic Journey

![](https://www.nzportraitgallery.org.nz/sites/default/files/styles/event_page/public/Appo%20Hocton%2C%20the%20first%20Chinese%20New%20Zealander%2C1876.%20WE%20Brown%20Collection.jpg?itok=r6OD3sdS)

### Curator: 

Phoebe H. Li & John B. Turner

### Location: 

New Zealand Portrait Gallery Te Pukenga Whakaata

### Start date: 

21 November 2019

### End date: 

16 February 2020

### Hours of admission: 

10.30 - 4.30

### Cost of admission: 

Free

[Link](https://www.nzportraitgallery.org.nz/whats-on/being-chinese-in-aotearoa-a-photographic-journey)
