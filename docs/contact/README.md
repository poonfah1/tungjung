# Contact Details

## Poon Fah Association

```
Poon Fah Association of NZ Inc
150 Vivian Street
Wellington 6011
NEW ZEALAND

P.O. Box 6333
Marion Square
Wellington 6141 
NEW ZEALAND
email: poonfahassociation@gmail.com
```

## Membership

The financial year for the Association begins on 1 April. Membership is lifetime after payment of membership fee as decided by Committee.  Currently $25.

## Webmaster

```
Dr Graham Chiu 趙世榮
Email: graham.chiu@tungjung.nz
```
