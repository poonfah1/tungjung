# Welcome to the Poon Fah Association of New Zealand

Originally named the Poon Yu Association. First established in 1870 in South Island goldmining period. Original building in Greymouth. New Zealand Association formed in 1916. In May 1962 the Poon Yu Cheong Sin Welfare Association (charterer of the SS Ventnor) combined with it and was renamed the Poon Fah Association. Building purchased in Vivian St in 1972.

## Full Membership

All people whose ancestry are from the counties of Poon Yu and Fah Yern in Guangdong province in China are invited to apply for full membership to the Association.

## Associate Membership

People who do not qualify for full membership but wish to partake in the activities of the Tung Jung Association are welcome to apply for associate membership to the Association.  However, the constitution does not allow voting rights for associate members.  

Associate members may have ancestral homes outside of the Poon Fah areas, and do not need to have any Chinese ancestry.  

:orange_book: [Membership form 2024](/contact/??) 

:orange_book: [Google Form](https://forms.gle/??) to fill in web based membership form

:orange_book: [Constitution Revised May 2001](assets/poonfah.pdf)


 