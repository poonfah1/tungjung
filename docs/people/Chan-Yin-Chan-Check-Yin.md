# Chan Yin/Chan Check Yin 

Chan Yin was born in Ha Gee village in 1870, arrived in Wellington in 1896, and was naturalised in 1905. He moved to Ohakune in 1909, becoming one of the first Chinese New Zealanders to settle in the area. He bought land at Ohakune Junction and opened a store there. He brought out several members of his family to work for him, including his nephew Tommy Chan. He was also responsible for bringing several other Jung Seng people to the area. Eventually there were Chan Yin stores in most of the surrounding districts, including Rangataua, Pokaka, Horopito and Raetihi. Evidence of the esteem that he was held in by the wider population of the district was shown when he became lost during a hunting expedition in July 1911. After being lost for almost two days his wife posted a reward for anyone who could find him. Most of the local population joined in the search. When he was eventually found, half frozen, no one would accept the reward, so in gratitude he invited them all to a spree. The local newspaper recorded that 80 men turned up and a riotous time was had by all. So famous did this incident become that a poem was written to commemorate it, one verse of which goes as follows:

> While the waiters charged our glasses, which took a brace of shakes,
We also made a murderous charge upon the sandwiches and cakes.
A guest then rose to sing a song amidst a loud applause,
And he got a good reception from the crowd of wagging jaws;
Then someone soon proposed the health of Chan our smiling host,
And with the din of cheering it beat at King George's toast,
We stood with flowing glasses of whisky, beer and gin,
I heard a clink of glasses, and a chorus of "Chan Yin!"

He moved to Hong Kong around 1937 but sadly was caught there by the Japanese invasion in 1941 and suffered badly as a result. He died in Hong Kong in 1943, aged 73. One of his sons Jack Chan is well known as the former owner of Mr Chan's in Chaffers Street.
