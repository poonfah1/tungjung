---
# This is the header page written in YAML, it's called Matter
home: true
heroImage: /poonfah.webp # 
heroText: Poon Fah Association of NZ Inc (1916)
tagline: 新 西 蘭 番 花 會 館
actionText: 開門 Enter →
actionLink: /about/
features:
- title: Cultural
  details: Promote the culture of the peoples from the Poon Yu (Panyu 番禺区) and Fah Yern (??) districts of China
- title: Education
  details: Teach the spoken Guangdong dialect of China, and Chinese literacy
- title: Social
  details: Organise social events for members and celebrate Chinese customs
footer: Copyright © 2024-present Poon Fah Association of NZ Inc, Hosted on GitLab.io
# You need to edit this file to see the special announcements
# which come from components stored in /docs/.vuepress/components
---



