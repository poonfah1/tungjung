# Genealogy

The association has an interest in establishing the genealogies of its current and past membership.  As the membership ages more and more historical information is being lost to the ravages of time.  Ancestral village locations are also being forgotten, and jiapus (家谱)/ zupu (族谱) that have been maintained for millennia lost as the ancestral halls were torn down in the Cultural Revolution. This may still be happening today as the clans form a potentially powerful force in China. And who in New Zealand still knows their [ancestral poems](https://en.wikipedia.org/wiki/Generation_name) that allowed relatives to recognise each other over the centuries.  

The first emperor of the Sung Dynasty, Emperor Taizu (Zhao Kuangyin 趙匡胤) put it like this

![](assets/sung-dynasty-edict.png)

## Software

There is an abundance of both free and commercial genealogy software available. Also website genealogy hosting is available too though some of these sites expose your family tree to others both view and edit, and that may not be desirable.

However, data can often be exchanged using the [GEDCOM](https://en.wikipedia.org/wiki/GEDCOM) data format.

## Free Software

I suggest that people dip their toes into the water and start with using an open source package called [Gramps](https://gramps-project.org/blog/).  You can download versions for use on a number of different platforms.

If there is enough interest we can look at holding workshops on how to use this software.

## Links


[Siyi Genealogy Board](http://siyigenealogy.proboards.com/) - Very helpful people. Site is primarily for Guangdong region

[Talk Story about My China Roots](https://1882foundation.org/talkstory/talk-story-review-my-china-roots/) - 1882 Foundation

[Talk by Le Huihan](https://www.facebook.com/1882ProjectFoundation/videos/427122438077105/)
