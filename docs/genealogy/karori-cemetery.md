# Karori Cemetery Tung Jung Memorial

The Tung Jung memorial is located just shortly inside the Karori Cemetery past the main gates off Karori Road.

![](/genealogy/assets/tungjung-memorial.jpg)

and is where we meet yearly for the [Chung Yeung Festival](https://en.wikipedia.org/wiki/Double_Ninth_Festival), or Double Ninth Festival as it is known outside the Chinese community.

The meeting dates will be notified yearly via the [Association Newsletters](/newsletters/) and in the [Tung Jung Calendar](/calendar/). A small lunch is usually provided by the Association, and an informal dinner is often held at a local restaurant (attendees pay).  If you wish to attend, please contact the [Chinese Secretary](/committee/) so that catering is appropriate.

If you need to do any maintenance of family graves then you may wish to come earlier so that you are finished by the time we meet.

## Dates

* 2024 Sunday 20th October at 12:00 pm
* 2020 Sunday 25th October at 12:00 pm
* 2019 Sunday 13th October at 12:00 pm
* 2018 Sunday 17th October at 12:00 pm

## Location

GPS Coordinates: -41.277435, 174.749935

Google Maps [location](https://www.google.com/maps/place/41%C2%B016'38.8%22S+174%C2%B044'59.8%22E/@-41.277435,174.7493865,177m/data=!3m2!1e3!4b1!4m5!3m4!1s0x0:0x0!8m2!3d-41.277435!4d174.749935)
