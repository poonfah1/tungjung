module.exports = {
    title: 'Poon Fah NZ',
    description: 'Poon Fah Association of New Zealand Inc, 19??',
    base: '/',
    dest: 'public',
    head: [
        ['link', { rel: 'icon', href: '/favicon-32x32.png' }],
        ['meta', { name: "google-site-verification", content: "f5eSVxvrV5ZkTBB9PKv01HP1AuLIzUyaYkpWQ3zUDBs" }]
    ],
    // displays the UNIX timestamp(ms) of each file's last git commit at bottom 
    // of each page in an appropriate format
    // NOTE: only works if this is  git repo and there is at least 1 commit
    themeConfig: {
        repo: 'https://gitlab.com/poonfah1/tungjung',
        repoLabel: 'Edit!',
        // if your docs are in a different repo from your main project:
        docsRepo: 'https://gitlab.com/poonfah1/tungjung',
        // if your docs are not at the root of the repo:
        docsDir: 'docs',
        // if your docs are in a specific branch (defaults to 'master'):
        docsBranch: 'master',
        // defaults to false, set to true to enable
        editLinks: true,
        // custom text for edit link. Defaults to "Edit this page"
        editLinkText: 'Help us improve this page!',
        lastUpdated: 'Last Updated',
        nav: [
          { text: 'Home', link: '/' },
          { text: 'Calendar', link: '/calendar/' }        ],
        //sidebar: 'auto'
         sidebar: [
            {
                title: 'About',   // required
                path: '/about/',      // optional, which should be a absolute path.
                children: ['/about/']
            },
            {
                title: 'Coronavirus',   // required
                path: '/covid-19/',      // optional, which should be a absolute path.
                children: ['/covid-19/']
            },
            {
                title: 'PoonFah Calendar',
                path: '/calendar/',
                children: ['/calendar/', 'calendar/Chinese-classes.md', '/social/', 'calendar/calendar-2020.md', '/social/china-trip-2020.md']
            },
            {
                title: 'Committee',
                path: '/committee/',
                children: ['/committee/']
            },
            {
                title: 'Contact Us',
                path: '/contact/',
                children: ['/contact/']
            },
            {
                title: 'Wellington Events',
                path: '/events/',
                children: ['/events/']
            },
            {
                title: 'Geography',
                path: '/geography/',
                collapsable: true, // optional, defaults to true
                sidebarDepth: 2,    // optional, defaults to 1
                children: [
                    '/geography/',
                    '/villages/',
                    '/villages/sungai'
                ]
            },
            {
                title: 'History',
                path: '/history/',
                children: ['/history/']
            },  
            {
                title: 'Newsletters',
                path: '/newsletters/',
                children: ['/newsletters/']
            },
            {
                title: 'People',
                path: '/people/',
                sidebarDepth: 2,    // optional, defaults to 1
                children: [
                  '/people/',
                  '/portraits/'
                ]
            },
            {   title: 'Genealogy',
                path: '/genealogy/',
                children: ['/genealogy/']
            }
        ]
    },
/*
        sidebar: [
            ['/about/', 'About'],
            ['/calendar/', 'TungJung Calendar'],
            ['/committee/', 'Committee'],
            ['/contact/', 'Contact Us'],
            ['/events/', 'Local Events'],
            ['/geography/', 'Geography'],
            ['/villages/', 'Villages'],
            ['/history/', 'History'],
            ['/newsletters/', 'Newsletters'],
            ['/portraits/', 'Portraits'],
            ['/genealogy/', 'Genealogy']
        ]
    },
*/
    plugins: ['@vuepress/plugin-last-updated']
}

